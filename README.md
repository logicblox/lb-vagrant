Usage
=====

Drop this Vagrantfile in any LB project together with a (Linux 64-bit) LogicBlox tarball
(at the same level), e.g. put these files in your project:

* `Vagrantfile`
* `logicblox-linux-4.3.9.tar.gz` # or any other recent 4.x version

Install [Vagrant](http://www.vagrantup.com/), then `cd` into the directory
containing the Vagrantfile and run:

    $ vagrant up

This will create and start a VM with LogicBlox in it (the version from the tarball
you have in that same directory). And will automatically start LB services for you.

Once completed:

    $ vagrant ssh

To SSH into the VM. You will immediately end up in the folder that has your
project folder mounted (from the host machine). Since your environment
variables have already been set up, you can verify that LB is running:

    $ lb services status

To exit from the VM (from ssh) simply run:

    $ exit

When you're not logged into the VM it will still keep running, you can either
stop it or destroy it to clean it up (note that destroying is perfectly OK because
project files are not stored in the VM itself).

To destroy:

    $ vagrant destroy

To stop it (can be started later again with `vagrant up`):

    $ vagrant halt

Accessing LogicBlox configuration and logs
------------------------------------------

When you run `vagrant up` a `lb_deployment` directory is created within your
project with a `logs` and `config` directory. These contain the current logs
and configuration of the running LB instance. You can change the configuration
and restart LB let the changes take effect.


Accessing LB Web
----------------

You can access LB web via `http://localhost:8080` normally. However, if port 8080
was already in use when you ran `vagrant up`, it will have picked another port automatically.
Watch the `vagrant up` output for lines like:

    Fixed port collision for 8080 => 8080. Now on port 8081.

to see what the port was mapped to. Other ports that are automatically mapped:

* 8086 -> 8086

Tips
----

If you only want to run a single command within the VM, you can use:

    $ vagrant ssh -c 'your command'

For instance:

    $ vagrant ssh -c 'lb services status'

or:

    $ vagrant ssh -c 'lb config && make check'
